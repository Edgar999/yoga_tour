(function() {
	'use strick';

	let startBtn = document.getElementById('start');
	let budgetValue = document.getElementsByClassName('budget-value')[0];
	let dayBudgetValue = document.getElementsByClassName('daybudget-value')[0];
	let levelValue = document.getElementsByClassName('level-value')[0];
	let expensesValue = document.getElementsByClassName('expenses-value')[0];
	let optionalExpensesValue = document.getElementsByClassName('optionalexpenses-value')[0];
	let incomeValue = document.getElementsByClassName('income-value')[0];
	let monthsavingsValue = document.getElementsByClassName('monthsavings-value')[0];
	let yearsavingsValue = document.getElementsByClassName('yearsavings-value')[0];

	let expensesItem = document.getElementsByClassName('expenses-item');
	let expensesBtn = document.getElementsByTagName('button')[0];
	let optionalExpensesBtn = document.getElementsByTagName('button')[1];
	let countBtn = document.getElementsByTagName('button')[2];
	let optionalExpensesItem = document.querySelectorAll('.optionalexpenses-item');
	let incomItem = document.querySelector('.choose-income');
	let checkSavings = document.querySelector('#savings');
	let sumValue = document.querySelector('.choose-sum');
	let percentValue = document.querySelector('.choose-percent');
	let yearValue = document.querySelector('.year-value');
	let monthValue = document.querySelector('.month-value');
	let dayValue = document.querySelector('.day-value'); 
	let logo = document.querySelector('.logo');
	let btn = document.createElement('button');
	
	btn.addEventListener('click', function() {

		btn.textContent = 'russian' ;
		
	});
	
	logo.appendChild(btn);
	btn.classList.add('text');
	btn.textContent = 'Translate';

	console.log(logo);
	console.log(btn);


	let money;
	let time;

	startBtn.addEventListener('click', function() {
		time = prompt("Введите дату в формате YYYY-MM-DD", "");
		money = +prompt("Ваш бюджет на месяц?", "");

		while (isNaN(money) || money == "" || money == null) {
				money = +prompt ("Ваш бюджет на месяц?", ""); 
		}

		appData.budget = money;
		appData.timeData = time;
		budgetValue.textContent = money.toFixed();
		yearValue.value = new Date(Date.parse(time)).getFullYear();
		monthValue.value = new Date(Date.parse(time)).getMonth() + 1;
		dayValue.value = new Date(Date.parse(time)).getDate() ;

	});

	expensesBtn.addEventListener('click', function() {
		let sum = 0;
			for (let i = 0; i < expensesItem.length; i++) {
				let item1 = expensesItem[i].value;
				let item2 = expensesItem[++i].value;

				if ( typeof(item1) != null && typeof(item2) != null && item1 != "" && item2 != "" && item1.length < 50 ) {
						console.log ("done");
						appData.expenses[item1] = item2;
						sum += +item2;
				} else {
						console.log ("bad result");
						i--;
				}

			}
	expensesValue.textContent = sum;

	});

	optionalExpensesBtn.addEventListener('click', function() {
		for (let i = 0; i < optionalExpensesItem.length; i++) {
			let obj = optionalExpensesItem[i].value;
			appData.optionalExpenses[i] = obj;
			optionalExpensesValue.textContent += appData.optionalExpenses[i] + ', ';
	}

	});

	countBtn.addEventListener('click', function() {

		if(appData.budget != undefined) {
			appData.moneyPerDay = (appData.budget / 30).toFixed();
			dayBudgetValue.textContent = appData.moneyPerDay;

			if (appData.moneyPerDay < 100) {
				levelValue.textContent = "Это минимальный уровень достатка!";
			} else if (appData.moneyPerDay > 100 && appData.moneyPerDay < 2000) {
				levelValue.textContent = "Это средний уровень достатка!";
			} else if (appData.moneyPerDay > 2000) {
				levelValue.textContent = "Это высокий уровень достатка!";
			} else {
				levelValue.textContent = "Ошибочка...!";
			}
		}	else {
			dayBudgetValue.textContent = "Ошибочка...!!!!!!!!";
		}
		
	});

	incomItem.addEventListener('input', function() {
		let items = incomItem.value;
		appData.income = items.split(", ");
		incomeValue.textContent = appData.income;

	});

	checkSavings.addEventListener('input', function() {
			if(appData.savings === true) {
				appData.savings = false
			}	else {
				appData.savings = true
			}
	});

	sumValue.addEventListener('input', function() {
		if(appData.savings === true) {
			let sum = +sumValue.value;
			let percent = +percentValue.value;

			appData.monthIncome = sum / 100 / 12 * percent;
			appData.yearIncome = sum / 100 * percent;

			monthsavingsValue.textContent = appData.monthIncome.toFixed(1);
			yearsavingsValue.textContent = appData.yearIncome.toFixed(1);
		}
	});

	percentValue.addEventListener('input', function() {
		if(appData.savings === true) {
			let sum = +sumValue.value;
			let percent = +percentValue.value;

			appData.monthIncome = sum / 100 / 12 * percent;
			appData.yearIncome = sum / 100 * percent;

			monthsavingsValue.textContent = appData.monthIncome.toFixed(1);
			yearsavingsValue.textContent = appData.yearIncome.toFixed(1);
		}
	});
			
	let appData = {
		budget: money,
		timeData: time,
		expenses: {},
		optionalExpenses: {},
		income: [],
		savings: false
	};

})();
